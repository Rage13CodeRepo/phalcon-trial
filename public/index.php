<?php
use Phalcon\Di\FactoryDefault;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {

    /**
     * The FactoryDefault Dependency Injector automatically registers
     * the services that provide a full stack framework.
     */
    $di = new FactoryDefault();

    /**
     * Handle routes
     */
    include APP_PATH . '/config/router.php';

    /**
     * Read services
     */
    include APP_PATH . '/config/services.php';

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();

    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/loader.php';

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);
    $application->registerModules([
        'users' => [
            'className' => 'App\Modules\Users\Module',
            'path' => APP_PATH.'/modules/users/module.php'
        ],
        'admin' => [
            'className' => 'App\Modules\Admin\Module',
            'path' => APP_PATH .'/modules/admin/module.php'
        ],
        'store' => [
            'className' => 'App\Modules\Store\Module',
            'path' => APP_PATH .'/modules/store/module.php'
        ]
    ]);

    echo str_replace(["\n","\r","\t"], '', $application->handle()->getContent());

} catch (\Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
