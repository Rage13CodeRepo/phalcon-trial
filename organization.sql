-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2019 at 12:45 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `organization`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` varchar(10) NOT NULL,
  `admin_email` varchar(255) DEFAULT NULL,
  `admin_password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_email`, `admin_password`) VALUES
('1', 'admin@organization.com', '$2y$12$NHVRUlBmSmxiM0tMSjRnRORaEuDDhepdkxh8mLVT4YuBFPWnqoHY6');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(4) NOT NULL,
  `emp_name` varchar(50) DEFAULT NULL,
  `job_id` int(4) NOT NULL,
  `emp_email` varchar(100) DEFAULT NULL,
  `emp_contactNumber` varchar(10) DEFAULT NULL,
  `emp_password` varchar(100) NOT NULL,
  `office_id` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `emp_name`, `job_id`, `emp_email`, `emp_contactNumber`, `emp_password`, `office_id`) VALUES
(8, 'Saish Sk', 7, 'srage13@organization.com', '9850670654', '$2y$12$ajhpVjZrSjVsTVVuUjhaaujPl3lwChSGTW01pd4LAHNHR7zCZD7YC', 9),
(9, 'Elliot Alderson', 8, 'elliot@organization.com', '7898665489', '$2y$12$NWtSUFpnN0VRODg2OTdMYumqAVeKQf2QdxTn9T0um9Lt64zD8Jjey', 10),
(10, 'Darlene Alderson', 9, 'darlene@organization.com', '7898568568', '$2y$12$U1JQbmZLRVlpRjFEd3orZeB/9J//I6fTxrmYyW98asSaKfJhoxFaK', 8),
(11, 'Rick Sanchez', 8, 'rick@organization.com', '8799878902', '$2y$12$R3Fwems0UTNZWjk3OEZLa.gKUvFsGQb8kSf3Uu9I0N4WOsukwo6vO', 9),
(12, 'Morty Smith', 7, 'morty@organization.com', '9856885600', '$2y$12$dWhTN0hZeE5qajNtcVE5QexP7zp0VnWtfRiO82OIB28zik2hhlum6', 9),
(13, 'Summer Smith', 9, 'summer@organization.com', '9854780002', '$2y$12$OUVNU2RFV284aVlLRGc1QumwwM.AqTds7anqsCRC.XUK/RBIzyJsW', 10),
(14, 'Walter White', 10, 'walter@organization.com', '9857600214', '$2y$12$dVZ1N2FQMVhEa0o4S0ZCS.RIO.JaRllzev2hbyKbeCYRbCFnFESoe', 8),
(15, 'Ringo Rancher', 10, 'ringo@organization.com', '8798654860', '$2y$12$SFAzN2ROcU1VQ25UUndVVuTvUIhr.C7hUZlzA7Lf3Tv8iQIYmN5ku', 8),
(16, 'Peter Parker', 7, 'peter@organization.com', '8978988589', '$2y$12$VFNobmxjRHJ4cjBhL3FPNOlE0LQofMNeKNtNf9JvN/oK.5WOV/Y3O', 10),
(17, 'Barry', 10, 'barry@organization.com', '8879586654', '$2y$12$ZktxTFFzUnNRZ2k3MzQ4S.X2x0QEzhytlwRH0Sl..caHeBVG0bqDy', 11);

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `id` int(4) NOT NULL,
  `job_title` varchar(30) DEFAULT NULL,
  `job_description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`id`, `job_title`, `job_description`) VALUES
(7, 'Developer', 'Developer Job'),
(8, 'Network Architect', 'Network Architect Job'),
(9, 'Front End Developer', 'Front End Developer Job'),
(10, 'Business Analyst ', 'Business Analyst Job');

-- --------------------------------------------------------

--
-- Table structure for table `office`
--

CREATE TABLE `office` (
  `id` int(4) NOT NULL,
  `office_name` varchar(50) DEFAULT NULL,
  `office_location` varchar(50) DEFAULT NULL,
  `office_contactNumber` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `office`
--

INSERT INTO `office` (`id`, `office_name`, `office_location`, `office_contactNumber`) VALUES
(8, 'New Office 1', 'Some far off location', '9986665898'),
(9, 'Old Office 1', 'Location of the old office', '8978980032'),
(10, 'New Office 2 ', 'New Office 2 Location', '7899878003'),
(11, 'Old Office 2', 'Old Office Location Duh!', '9864589000'),
(12, 'New Office 3', 'New Office Loaction', '9850646589'),
(13, 'Old Office 3', 'Old Office Loaction', '9576486502');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(4) NOT NULL,
  `product_name` varchar(150) DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `product_type` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `product_image`, `product_type`) VALUES
(1, 'Some Product Name', 'public/products/410633.jpg', 'Product Type');

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `id` int(4) NOT NULL,
  `task_title` varchar(30) DEFAULT NULL,
  `task_priority` int(1) DEFAULT NULL,
  `task_image` varchar(255) DEFAULT NULL,
  `task_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`id`, `task_title`, `task_priority`, `task_image`, `task_description`) VALUES
(10, 'Change Background', 2, 'public/tasks/437283.png', 'Change Background to some HEXCODE'),
(11, 'Change Image Theme', 4, 'public/tasks/153617.jpg', 'New Image Theme Description: ...'),
(12, 'Make a Similar Image', 1, 'public/tasks/792086.png', 'The Requirements...'),
(13, 'Some Title', 3, 'public/tasks/620335.png', 'Do it'),
(14, 'Replicate this screen shot', 4, 'public/tasks/13314.png', 'Instructions\r\n\r\n1) ...\r\n2) ...\r\n3) ...'),
(15, 'Try this out', 1, 'public/tasks/972440.png', 'Instructions'),
(16, 'What is this', 2, 'public/tasks/875052.png', 'Wha What ...!'),
(17, 'Some random task', 2, 'public/tasks/561846.jpg', 'Random Task'),
(18, 'Another Random Task', 2, 'public/tasks/196653.png', 'Do this');

-- --------------------------------------------------------

--
-- Table structure for table `taskassigned`
--

CREATE TABLE `taskassigned` (
  `id` int(4) NOT NULL,
  `employee_id` int(4) DEFAULT NULL,
  `task_id` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taskassigned`
--

INSERT INTO `taskassigned` (`id`, `employee_id`, `task_id`) VALUES
(1, 8, 10),
(2, 9, 10),
(3, 9, 11),
(4, 10, 12),
(5, 13, 12),
(6, 13, 11),
(7, 14, 11),
(8, 15, 10),
(9, 10, 10),
(10, 12, 10),
(11, 15, 11),
(12, 8, 11),
(13, 14, 10),
(14, 8, 13),
(15, 10, 13),
(16, 9, 13),
(17, 12, 14),
(18, 8, 14),
(19, 16, 14),
(20, 8, 12),
(21, 16, 10),
(22, 9, 14);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `office_id` (`office_id`),
  ADD KEY `job_id` (`job_id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `office`
--
ALTER TABLE `office`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taskassigned`
--
ALTER TABLE `taskassigned`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `task_id` (`task_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `office`
--
ALTER TABLE `office`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `taskassigned`
--
ALTER TABLE `taskassigned`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`office_id`) REFERENCES `office` (`id`),
  ADD CONSTRAINT `employee_ibfk_2` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`);

--
-- Constraints for table `taskassigned`
--
ALTER TABLE `taskassigned`
  ADD CONSTRAINT `taskassigned_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`),
  ADD CONSTRAINT `taskassigned_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
