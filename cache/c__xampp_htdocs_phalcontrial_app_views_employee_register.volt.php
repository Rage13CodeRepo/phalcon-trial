<div class="-container-fluid" >
    <form class = "" action="submit" method = "POST">
        <div class="form-group">
            <label for="name">Name</label>
            <input class = "form-control" type="text" name="name" id="name">
        </div>
        <div class="form-group">
            <label for="designation">Designation</label>
            <input type="text" name="designation" id="designation" class="form-control">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input class = "form-control" type="text" name="email" id="email">
        </div>
        <div class="form-group">
            <label for="contactNumber">Contact Number</label>
            <input class = "form-control" type="text" name="contactNumber" id="contactNumber">
        </div>
        <div class="form-group">
            <label for="contactNumber">Contact Number</label>
            <select class = "form-control" name="office" id="office">
                <option>-- Select Office --</option>
                <option>Office 1</option>
                <option>Office 2</option>
                <option>Office 3</option>
            </select>
        </div>
        <div class="form-group">
            <input type="submit" value="SUBMIT" class = "btn btn-primary">
        </div>
    </form>
</div>