<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Phalcon PHP Framework</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= $this->url->get('') ?>">Organiztion</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?= $this->url->get('') ?>">Home</a></li>
                <li><a href="<?= $this->url->get('employee/register') ?>">Add Employee</a></li>
                <li><a href="<?= $this->url->get('office/addoffice') ?>">Add Office</a></li>
                <li><a href="<?= $this->url->get('employee/view') ?>">View Employee</a></li>
            </ul>
        </div>
    </nav>
    <div class="container" style = "margin-top:30px">
        <?= $this->getContent() ?>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>

</html>