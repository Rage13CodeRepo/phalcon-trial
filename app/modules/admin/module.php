<?php 

    namespace App\Modules\Admin;

    use Phalcon\Loader;
    use Phalcon\Mvc\View;
    use Phalcon\Mvc\ModuleDefinitionInterface;
    use Phalcon\Mvc\Dispatcher;
    use Phalcon\DiInterface;

    /**
     * We have to register the namespace, services used for the module
     * The module.php helps configure the applications for HMVC applications 
     */

    class Module implements ModuleDefinitionInterface {
        /**
         * Registering Namespace to be used by the module.
         */
        
        public function registerAutoloaders(DiInterface $di = null) {

            $loader = new Loader();
            $loader->registerNamespaces([
                'App\Modules\Admin\Controllers' => APP_PATH . '/modules/admin/controllers',
                'App\Modules\Admin\Forms' => APP_PATH . '/modules/admin/forms'
            ]);
            $loader->register();
        }

        /**
         * Registering the services to be used by the module.
         */
        public function registerServices(DiInterface $di) {
            /**
             * Register the dispatcher
             * instantiate a disptacher, set the default namespace and then return the dispatcher
             */
            $di->set('dispatcher', function() {
                $dispatcher = new Dispatcher();
                $dispatcher->setDefaultNamespace('App\Modules\Admin\Controllers');
                return $dispatcher;
            });

            /**
             * Register the view component
             * instatntiate a view, set the view directory and then return the view
             */
            $di->set('view', function(){
                $view = new View();
                $view->setViewsDir(APP_PATH.'/modules/admin/views');
                $view->setMainView(APP_PATH . '/templates/layouts/index');
                $view->setPartialsDir(APP_PATH. '/templates/layouts/partials/admin/');
                return $view;
            });
        }
    }