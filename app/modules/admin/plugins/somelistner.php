<?php

    namespace App\Modules\Admin\Plugin;

    use Phalcon\Events\Event;

    class SomeListner {
        public function beforeSomeTask(Event $event, $component) {
            echo "Before Task Listner";
        }

        public function afterSomeTask(Event $event, $component) {
            echo "After Task Listner";
        }

    }