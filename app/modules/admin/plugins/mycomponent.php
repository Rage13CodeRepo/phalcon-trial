<?php

    namespace App\Modules\Admin\Plugin;

    use Phalcon\Events\EventsAwareInterface;
    use Phalcon\Events\ManagerInterface;

    class MyComponent implements EventsAwareInterface {
        protected $eventsManager;

        public function setEventManager(ManagerInterface $eventsManager) {
            $this->eventsManager = $eventsManager;
        }

        public function getEventsManager() {
            return $this->eventsManager;
        }

        public function someTask() {
            $this->eventsManager->fire('my-comopenet:beforeSomeTask', $this);
            echo "Just Did Some Task";
            $this->eventManager->fire('my-component:afterSomeTask', $this);
        }
    }