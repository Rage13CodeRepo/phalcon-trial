<?php

    namespace App\Modules\Admin\Controllers;

    /**
     * When you use HMVC it is necessary add the namespace as above
     */

    use App\Library\ControllerBase;
    use App\Model\Office;
    use App\Model\Employee;
    use App\Model\Job;
    use App\Modules\Admin\Forms\EditEmployeeForm;
    use App\Modules\Admin\Forms\EmployeeForm;
    use App\Library\Utility;
    use Phalcon\Paginator\Adapter\Model as PaginatorModel;
    use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
    

    class EmployeeController extends ControllerBase {
        
        public function indexAction() {
            
        }

        public function registerAction() {

            $employeeObj = Employee::find();      
            $officeListObj = Office::find();
            $jobListObj = Job::find();
            foreach($officeListObj as $ol) {
                $officeList[$ol->id] = $ol->office_name; 
            }
            foreach($jobListObj as $jl) {
                $jobList[$jl->id] = $jl->job_title; 
            }
            // print_r($officeList);
            // print_r($jobList);
            // die();

            $form = new EmployeeForm();
            if($this->request->isPost()) {
                if($form->isValid($this->request->getPost()) != false) {
                    $request = $this->request->getPost();
                    $employee = new Employee();
                    $employee->setEmp_name($request['emp_name']);
                    $employee->setJob_id($request['job_id']);
                    $employee->setEmp_contactNumber($request['emp_contactNumber']);
                    $employee->setEmp_email($request['emp_email']);
                    $employee->setEmp_password($this->security->hash('user@123'));
                    $employee->setOffice_id($request['office_id']);
                    // Utility::PrintR($request);
                    $employee->save();
                    $this->response->redirect('employee/register');
                }
                else {
                    $errors = $form->getMessages();
                }
            }
            $this->view->jobList = $jobList;
            $this->view->officeList = $officeList;
            $this->view->errors = $errors ? $errors : NULL;
            $this->view->form = $form;
            $this->view->employees = $employeeObj;
        }

        public function viewAction($id) {
            
            $employeeObj = Employee::findFirst($id);
            // you can use the alias defined in the relations of the 
            // models to call for the related models.
            $officeName = $employeeObj->office->getOffice_name();
            $jobTitle = $employeeObj->job->getJob_title();
            $this->view->setVars([
                'employeeObj' => $employeeObj,
                'officeName' => $officeName,
                'jobTitle' => $jobTitle,
            ]);
        }
        
        public function submitAction() {
            
        }

        public function viewallAction() {
            $currentPage = $this->request->get('page') ? $this->request->get('page'): 1 ;
            $serachFilter = $this->request->get('search') ? '%'.$this->request->get('search').'%': '%%';
            $builder = $this->modelsManager->createBuilder();
            $builder->from('App\Model\Employee');
            $builder->where('emp_name like :searchFilter:', ['searchFilter' => $serachFilter]);
            $paginator = new PaginatorQueryBuilder([
                'builder' => $builder,
                'limit' => 5,
                'page' => $currentPage,
            ]);
            $page = $paginator->getPaginate();
            $this->view->setVars([
                'page' => $page,
                'searchFilter' => $this->request->get('search')
            ]);
        }

        public function updateAction($id) {
            $columnToChange = $this->request->getPost("columnToChange");
            $changedValue = $this->request->getPost("changedValue");
            $this->db->begin();
            $employee = new Employee;
            $employee = Employee::findFirst($id);
            $field = "set".ucfirst($columnToChange);
            $employee->$field($changedValue);
            if($employee->save() === FALSE) {
                $this->db->rollback();
            }
            $this->db->commit();
        }

        public function editAction($id) {
            $employeeObj = Employee::findFirst($id);
            $this->view->setVar('employeeObj', $employeeObj);
            /**
             * add an array as an additional parameter, with edit value set as true.
             */
            $form = new EditEmployeeForm($employeeObj, ['edit' => true]);
            $this->view->form = $form;
        }

        public function deleteAction($id) {
            $employee = Employee::findFirst($id);
            
            $employee->delete($id);
            $this->response->redirect('employee/viewall');
        }

        public function resetpasswordAction($id) {
            $defaultPassword = "user@123";
            $employeeObj = Employee::findFirst($id);
            $encPassword = $this->security->hash($defaultPassword);
            $employeeObj->setEmp_password($encPassword);
            $employeeObj->save();
            $this->view->disable();
            $this->response->redirect('employee/view/'.$id);
        }
    }