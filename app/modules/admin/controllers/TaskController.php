<?php

    namespace App\Modules\Admin\Controllers;

    use App\Model\Task;
    use App\Model\Taskassigned;
    use App\Model\Employee;
    use App\Modules\Admin\Forms\TaskForm;
    use App\Library\ControllerBase;
    use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use App\Library\Utility;

class TaskController extends ControllerBase {
        
        public function indexAction() {
            
        }

        public function addtaskAction() {
            $form = new TaskForm();
            if($this->request->isPost()) {
                if($form->isValid($this->request->getPost()) != false) {
                    $allowedFileTypes = ['jpeg','jpg','png','gif'];
                    $request = $this->request->getPost();
                    foreach($this->request->getUploadedFiles() as $file) {
                        if(in_array($file->getExtension(), $allowedFileTypes)) {
                            $storagePath = "/../../../../public/tasks/";
                            $tmpName = rand('1','999999').'.'.$file->getExtension();
                            $tmpFileStoragePath = dirname(__FILE__).$storagePath.$tmpName;
                            $fileStoragePath = "public/tasks/".$tmpName;
                            if($file->moveTo($tmpFileStoragePath)) {
                                $task = new Task();
                                $task->setTask_title($request['task_title']);
                                $task->setTask_priority($request['task_priority']);
                                $task->setTask_image($fileStoragePath);
                                $task->setTask_description($request['task_description']);
                                $task->save();
                            }
                        }
                        else {

                        }
                        $this->response->redirect('task/addtask');
                    }

                }
                else {
                    $errors = $form->getMessages();
                }
            }
            $this->view->errors = $errors;            
            $this->view->form = $form;
        }

        public function assigntaskAction($id) {
            //$taskAssignedDetailObj = Task::getTaskDetails($id);
            $taskObj = Task::find($id);
            $employeeObj = Employee::find();
            $taskAssignedToObj = Taskassigned::getEmployeeNameAssignedToTask($id);
            // echo "<pre>";
            // var_dump($taskAssignedToObj);
            // echo "</pre>";
            // die();
            $this->view->setVars([
                'taskObj'=> $taskObj,
                'employeeObj' => $employeeObj,
                'taskAssignedToObj' => $taskAssignedToObj
            ]);
        }

        public function viewtaskAction() {
            $currentPage = $this->request->get('page') ? $this->request->get('page') : 1;
            $searchFilter = $this->request->get('search') ? '%'.$this->request->get('search').'%' : '%%';
            $builder = $this->modelsManager->createBuilder();
            $builder->from('App\Model\Task');
            $builder->where('task_title like :searchFilter:', ['searchFilter' => $searchFilter]);
            $paginator = new PaginatorQueryBuilder([
                'builder' => $builder,
                'limit' => 5,
                'page' => $currentPage,
            ]);
            $page = $paginator->getPaginate();
            $this->view->setVars([
                'page' => $page,
                'searchFilter' => $this->request->get('search')
            ]);
                
        }

        public function submitAction() {
            
        }

    }

?>