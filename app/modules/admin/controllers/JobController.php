<?php

    namespace App\Modules\Admin\Controllers;

    use App\Model\Job;
    use App\Modules\Admin\Forms\JobsForm;
    use App\Library\ControllerBase;

    class JobController extends ControllerBase {

        public function indexAction() {

        }
        public function addjobAction() {
            $form = new JobsForm();
            $errors = [];
            if($this->request->isPost()) {
                if($form->isValid($this->request->getPost()) != false) {
                    $request = $this->request->getPost();
                    $job = new Job();
                    $job->setJob_title($request["job_title"]); 
                    $job->setJob_description($request["job_description"]); 
                    $job->save();        
                }
                else {
                    $errors = $form->getMessages();
                }
            }
            $this->view->errors = $errors;
            $this->view->form = $form;
        }

        public function submitAction() {
            
        }

        public function getJobLists() {
            $jobs = Job::getJobList();
        }
    }

?>