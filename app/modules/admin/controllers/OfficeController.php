<?php

    namespace App\Modules\Admin\Controllers;

    use App\Model\Office;
    use App\Library\ControllerBase;
    use App\Modules\Admin\Forms\OfficeForm;

class OfficeController extends ControllerBase {
        
        public function indexAction() {
            
        }

        public function addofficeAction() {
            $form = new OfficeForm();
            if($this->request->isPost()) {
                if($form->isValid($this->request->getPost()) != false) {
                    $request = $this->request->getPost();
                    $office = new Office();
                    $office->setOffice_name($request['office_name']);
                    $office->setOffice_location($request['office_location']);
                    $office->setOffice_contactNumber($request['office_contactNumber']);
                    $office->save();
                    $this->response->redirect('office/addoffice');
                }
                else {
                    $errors = $form->getMessages();
                }
            }
            $this->view->errors = $errors;
            $this->view->form = $form;
        }

        public function viewAction($id) {
            
        }

        public function submitAction() {
            
        }

        public function getOfficeListAction() {
            $officeObj = Office::getOfficeList();
        }
    }
