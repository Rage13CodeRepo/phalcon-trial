<?php

    namespace App\Modules\Admin\Controllers;

    use App\Library\ControllerBase;
    use App\Model\Admin;
    use App\Modules\Admin\Forms\AdminLoginForm;

    class IndexController extends ControllerBase {
        /**
         * 
         * By default Phalcon will always use the index controller as the start
         * point for any application. It is set in the Phalcon/Mvc/Dispatcher
         * < -- Check git hub cphalcon-- >
         * 
         */

        public function indexAction() {
            $this->disptacher->forward(['controller' => 'index', 'action' => 'home']);
        }

        public function homeAction() {
            
        }

        public function loginAction() {
            $form = new AdminLoginForm();
            $errors = null;
            if($this->request->isPost()) {
                if($form->isValid($this->request->getPost()) != false) {
                    $email = $this->request->getPost('admin_email');
                    $password = $this->request->getPost('admin_password');
                    $adminObj = Admin::findFirst([
                        "conditions" => "admin_email = '". $email . "'"
                    ]);
                    if($adminObj) {
                        if($this->security->checkHash($password, $adminObj->admin_password)) {
                            $AdminData = [
                                'AdminId' => $adminObj->admin_id,
                                'AdminEmail' => $adminObj->admin_email
                            ];
                            $this->SessionData->setAdminData($AdminData);
                            $this->cookies->set('AdminData', $AdminData);
                            $this->response->redirect('');
                        }
                        else {
                            $this->flashSession->error('Incorrect password');
                        }
                    }
                    else {
                        $this->flashSession->error('Incorrect email and password');
                    }
                }
                else {
                    $errors = $form->getMessages();
                }
            }
            $this->view->errors = $errors;
            $this->view->form = $form;
        }

        public function LogoutAction() {
            $this->session->remove('AdminData');
            $this->response->redirect('');
        }

    }