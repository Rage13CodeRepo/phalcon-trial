<?php

    namespace App\Modules\Admin\Controllers;

    use App\Modules\Admin\Forms\AddProductForm;
    use App\Library\ControllerBase;
    use App\Model\Product;
    use App\Library\Utility;

    class ProductController extends ControllerBase {
        public function indexAction() {

        }

        public function addproductAction() {
            $form = new AddProductForm();
            $errors = [];
            if($this->request->isPost()) {
                // Utility::VarDump($form->isValid($this->request->getPost()));
                if($form->isValid($this->request->getPost()) != false) {
                    $allowedFileTypes = ['jpeg','jpg','png','gif'];
                    $request = $this->request->getPost();
                    foreach($this->request->getUploadedFiles() as $file) {
                        if(in_array($file->getExtension(), $allowedFileTypes)) {
                            $storagePath = "/../../../../public/products/";
                            $tmpName = rand('1','999999').'.'.$file->getExtension();
                            echo $tmpFileStoragePath = dirname(__FILE__).$storagePath.$tmpName;
                            $fileStoragePath = "public/products/".$tmpName;
                            if($file->moveTo($tmpFileStoragePath)) {
                                $product = new Product();
                                $product->setProduct_name($request['product_name']);
                                $product->setProduct_image($fileStoragePath);
                                $product->setProduct_type($request['product_type']);
                                $product->save();
                            }
                        }
                        $this->response->redirect('product/addproduct');
                    }                                    
                }
                else {
                    $errors = $form->getMessages();
                }
            }
            $this->view->form = $form;
            $this->view->errors = $errors;
        }
    }