<?php 

    use App\Library\ControllerBase;
    use App\Model\Employee;
    use Phalcon\Paginator\Adapter\Model as PaginatorModel;
    use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;
    use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
    
    class PaginatorController extends ControllerBase {

        public function indexAction() {
            $page = $this->request->get('page') ? $this->request->get('page') : 1;
            $employeeObj = Employee::find();
            $paginator = new PaginatorModel([
                'data' => $employeeObj,
                'limit' => 5,
                'page' => $page, 
            ]);

            $page = $paginator->getPaginate();
            $this->view->setVar('page', $page);

        }

    }
