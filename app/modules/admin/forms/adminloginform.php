<?php

    namespace App\Modules\Admin\Forms;

    use App\Library\BaseForm;
    use Phalcon\Forms\Element\Text;
    use Phalcon\Forms\Element\Password;
    use Phalcon\Forms\Element\Submit;
    use Phalcon\Validation\Validator\Email;
    use Phalcon\Validation\Validator\PresenceOf;
    use Phalcon\Validation\Validator\Alpha;

    class AdminLoginForm extends BaseForm {
        private function AdminEmail() {
            $element = new Text('admin_email');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','AdminPassword');
            $element->setLabel('Email');
            $element->addValidator(
                new Email(["message" => "You haven't entered a valid email address"])
            );
            $this->add($element);
        }

        private function AdminPassword() {
            $element = new Password('admin_password');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','AdminName');
            $element->setLabel('Password');
            $element->addValidator(
                new PresenceOf(["message" => "Password is a required field"])
            );
            $this->add($element);
        }
        
        private function Submit() {
            $element = new Submit('submit');
            $element->setAttribute('class','btn btn-primary col-md-12');
            $element->setAttribute('id','Login');
            $element->setAttribute('value','Login');
            $this->add($element);
        }

        public function initialize() {
            $this->AdminEmail();
            $this->AdminPassword();
            $this->Submit();
        }
    }