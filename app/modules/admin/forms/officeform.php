<?php

    namespace App\Modules\Admin\Forms;

    use Phalcon\Forms\Element\Text;
    use Phalcon\Forms\Element\Submit;
    use Phalcon\Validation\Validator\PresenceOf;
    use App\Library\BaseForm;

    class OfficeForm extends BaseForm {

        public function OfficeName() {
            $element = new Text('office_name');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','OfficeName');
            $element->setLabel('Office Name');
            $element->addValidator(
                new PresenceOf(['message' => 'Office name is an required field'])
            );
            $this->add($element);
        }

        public function OfficeLocation() {
            $element = new Text('office_location');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','OfficeLocation');
            $element->setLabel('Office Location');
            $element->addValidator(
                new PresenceOf(['message' => 'Office location is an required field'])
            );
            $this->add($element);
        }

        public function OfficeContactNumber() {
            $element = new Text('office_contactNumber');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','OfficeContactNumber');
            $element->setLabel('Office Contact Number');
            $element->addValidator(
                new PresenceOf(['message' => 'Office contact number is an required field'])
            );
            $this->add($element);
        }

        public function Submit() {
            $element = new Submit('submit');
            $element->setAttribute('class', 'btn btn-primary');
            $element->setAttribute('value', 'Submit');
            $element->setAttribute('id', 'submit');
            $this->add($element);
        }

        public function initialize() {
            $this->OfficeName();
            $this->OfficeLocation();
            $this->OfficeContactNumber();
            $this->Submit();
        }
    }
