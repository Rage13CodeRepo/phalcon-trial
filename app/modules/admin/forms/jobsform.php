<?php 

    namespace App\Modules\Admin\Forms;

    use App\Library\BaseForm;
    use Phalcon\Forms\Form;
    use Phalcon\Forms\Element\Text;
    use Phalcon\Forms\Element\TextArea;
    use Phalcon\Forms\Element\Submit;
    use Phalcon\Validation\Validator\PresenceOf;

    class JobsForm extends BaseForm {
        
        private function JobTitle() {
            $element = new Text('job_title');
            $element->setAttribute('id', 'JobTitle');
            $element->setAttribute('class', 'form-control');
            $element->setLabel('Job Title');
            $element->addValidator(
                new PresenceOf(['message' => 'Job Title is an required field'])
            );
            $this->add($element);
        }
        
        private function JobDescription() {
            $element = new TextArea('job_description');
            $element->setAttribute('class', 'form-control');
            $element->setAttribute('cols', '10');
            $element->setAttribute('rows', '10');
            $element->setAttribute('id','JobDescription');
            $element->setLabel('Job Description');
            $element->addValidator(
                new PresenceOf(['message' => 'Job Description is an required field'])
            );
            $this->add($element);
        }
        
        private function Submit() {
            $element = new Submit('submit');
            $element->setAttribute('id','submit');
            $element->setAttribute('class', 'btn btn-primary');
            $element->setAttribute('value', 'Submit');
            $this->add($element);
        }

        public function initialize() {
            $this->JobTitle();
            $this->JobDescription();
            $this->Submit();
        }
    }

?>