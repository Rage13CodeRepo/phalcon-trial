<?php 

    namespace App\Modules\Admin\Forms;

    use Phalcon\Forms\Form;
    use Phalcon\Forms\Element\Text;
    use Phalcon\Forms\Element\TextArea;
    use Phalcon\Forms\Element\Select;
    use Phalcon\Forms\Element\File;
    use Phalcon\Forms\Element\Submit;
    use App\Library\BaseForm;
    use Phalcon\Validation\Validator\PresenceOf;
    use Phalcon\Validation\Validator\File as FileValidator;

    class TaskForm extends BaseForm {

        private function TaskTitle() {
            $element = new Text('task_title');
            $element->setLabel('Task Title');
            $element->setAttribute('placeholder','Task Title');
            $element->setAttribute('class','form-control');
            $element->addValidator(
                new PresenceOf(['message' => 'The Task Title is an required field'])
            );
            $this->add($element);
        }

        private function TaskPriority() {
            $element = new Select('task_priority', [
                '' => '-- Select Priority --',
                '1' => 'Low',
                '2' => 'Medium',
                '3' => 'Important',
                '4' => 'Critical'
            ]);
            $element->setLabel('Priority');
            $element->setAttribute('class','form-control');
            $element->setAttribute('placeholder', 'Select Priorty');
            $element->addValidator(
                new PresenceOf(['message' => 'The Task Priority is an required field'])
            );
            $this->add($element);
        }

        private function TaskDescription() {
            $element = new TextArea('task_description');
            $element->setLabel('Task Description');
            $element->setAttribute('class','form-control');
            $element->setAttribute('cols','10');
            $element->setAttribute('rows','10');
            $element->setAttribute('placeholder', 'Task Description');
            $element->addValidator(
                new PresenceOf(['message' => 'The Task Description is an required field'])
            );
            $this->add($element);
        }

        private function TaskFile() {
            $element = new File('task_file');
            $element->setAttribute('id','taskFile');
            $element->setLabel('Task File');
            $element->setAttribute('class','form-control');
            $element->setAttribute('style','border:none');
            // $element->addValidator(
            //     new FileValidator([
            //         "allowedTypes" => [
            //             "file" => [
            //                 "image/jpeg",
            //                 "image/jpg",
            //                 "image/png", 
            //                 "image/gif", 
            //             ]
            //         ],
            //         "messageType" => [
            //             "file" => "Only jpeg, png and gif files allowed"
            //         ]
            //     ])
            // );
            $this->add($element);
        }

        private function Submit() {
            $element = new Submit('submit');
            $element ->setAttribute('id','submit');
            $element ->setAttribute('name','submit');
            $element ->setAttribute('value','Submit');
            $element ->setAttribute('class','btn btn-primary');
            $this->add($element);
        }
        
        public function initialize() {
            $this->TaskTitle();
            $this->TaskPriority();
            $this->TaskDescription();
            $this->TaskFile();
            $this->Submit();
        }

    }

?>