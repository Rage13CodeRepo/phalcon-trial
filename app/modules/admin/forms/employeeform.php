<?php 

    namespace App\Modules\Admin\Forms;
    use App\Library\BaseForm;
    use Phalcon\Forms\Element\Text;
    use Phalcon\Forms\Element\Select;
    use Phalcon\Forms\Element\Submit;
    use Phalcon\Validation\Validator\PresenceOf;
    use Phalcon\Validation\Validator\Alpha;
    use Phalcon\Validation\Validator\Email;
    use Phalcon\Validation\Validator\StringLength;
    use Phalcon\Validation\Validator\Digit;
    

    class EmployeeForm extends BaseForm {
        
        private function EmployeeName() {
            $element = new Text('emp_name');
            $element->setLabel('Name');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','name');
            $element->addValidator(
                new PresenceOf(['message' => ' Employee name is a required field'])
            );
            $element->addValidator(
                new Alpha(['message' => ' Employee name should only have alphabets'])
            );
            $this->add($element);
        }

        private function EmployeeDesignation() {
            $element = new Select('job_id');
            $element->setLabel('Designation');
            $element->addOption(['id' => '','value' => ' -- Choose Designation --']);
            $element->setAttribute('class', 'form-control');
            $element->setAttribute('id', 'designation');
            $this->add($element);
        }

        private function EmployeeEmail() {
            $element = new Text('emp_email');
            $element->setLabel('Email');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','email');
            $element->addValidator(
                new PresenceOf(['message' => ' Email is an required field'])
            );
            $element->addValidator(
                new Email(['message' => ' You have not entered a valid email address'])
            );
            $this->add($element);
        }

        private function EmployeeContactNumber() {
            $element = new Text('emp_contactNumber');
            $element->setLabel('Contact Number');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','contactnumber');
            $element->addValidator(
                new PresenceOf(['message' => ' Contact number is an required field'])
            );
            $element->addValidator(
                new StringLength([
                    'min' => 10,
                    'max' => 10,
                    'messageMinimum' => ' You have not entered a valid mobile number',
                    'messageMaximum' => ' You have not entered a valid mobile number'
                ])
            );
            $element->addValidator(
                new Digit(['message' => ' You have not entered a valid mobile number'])
            );
            $this->add($element);
        }

        private function EmployeeOffice() {
            $element = new Select('office_id');
            $element->setLabel('Office');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','office');
            $this->add($element);
        }

        private function Submit() {
            $element = new Submit('submit');
            $element->setAttribute('class','btn btn-primary');
            $element->setAttribute('id','submit');
            $element->setAttribute('value','Submit');
            $this->add($element);
        }

        public function initialize() {
            $this->EmployeeName();
            $this->EmployeeDesignation();
            $this->EmployeeEmail();
            $this->EmployeeContactNumber();
            $this->EmployeeOffice();
            $this->Submit();
        }
    }

?>