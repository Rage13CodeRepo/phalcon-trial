<?php 

    namespace App\Modules\Admin\Forms;

    use Phalcon\Forms\Element\Text;
    use Phalcon\Forms\Element\File;
    use Phalcon\Forms\Element\Submit;
    use Phalcon\Validation\Validator\PresenceOf;
    use App\Library\BaseForm;

    class AddProductForm extends BaseForm {
        private function ProductName() {
            $element = new Text('product_name');
            $element->setAttribute('class', 'form-control');
            $element->setAttribute('id', 'ProductName');
            $element->setLabel('Product Name');
            $element->addValidator(
                new PresenceOf(['message' => 'Product name is an required field'])
            );
            $this->add($element);
        }

        private function ProductImage() {
            $element = new File('product_image');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','ProductImage');
            $element->setAttribute('style','border:none');
            $element->setLabel('Product Image');
            $this->add($element);
        }

        private function ProductType() {
            $element = new Text('product_type');
            $element->setAttribute('class', 'form-control');
            $element->setAttribute('id', 'ProductType');
            $element->setLabel('Product Type');
            $element->addValidator(
                new PresenceOf(['message' => 'Product type is an required'])
            );
            $this->add($element);
        }

        private function Submit() {
            $element = new Submit('submit');
            $element->setAttribute('class', 'btn btn-primary');
            $element->setAttribute('id', 'submit');
            $element->setAttribute('value', 'Submit');
            $this->add($element);
        }

        public function initialize() {
            $this->ProductName();
            $this->ProductImage();
            $this->ProductType();
            $this->Submit();
        }
    }