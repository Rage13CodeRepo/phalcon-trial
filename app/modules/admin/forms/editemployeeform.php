<?php

    namespace App\Modules\Admin\Forms;

    use Phalcon\Forms\Form;
    use Phalcon\Validation\Validator\PresenceOf;
    use Phalcon\Forms\Element\Text;
    use Phalcon\Forms\Element\Hidden;
    use App\Library\BaseForm;

    class EditEmployeeForm extends BaseForm {

        private function Id() {
            $element = new Hidden('id');
            $element->setAttribute('id','empid');
            $this->add($element);
        }

        private function EmployeeName() {
            $element = new Text('emp_name');
            $element->setLabel('Employee Name');
            $element->setAttribute('id','Name');
            $element->setAttribute('class','form-control textbox');
            $element->addValidator(
                new PresenceOf(['message' => 'The Employee name is an required field'])
            );
            $this->add($element);
        }
        
        private function EmployeeEmail() {
            $element = new Text('emp_email');
            $element->setLabel('Employee Email');
            $element->setAttribute('id', 'Email');
            $element->setAttribute('class','form-control textbox');
            $element->addValidator(
                new PresenceOf (['message' => 'The Emaployee email is an required field'])
            );
            $this->add($element);
        }
        
        private function EmployeeContactNumber() {
            $element =  new Text('emp_contactNumber');
            $element->setLabel('Employee Contact Number');
            $element->setAttribute('id','ContactNumber');
            $element->setAttribute('class','form-control textbox');
            $element->addValidator(
                new PresenceOf(['message' => 'The Employee contact number is an required field'])
            );
            $this->add($element);
        }

        public function initialize() {
            $this->Id();
            $this->EmployeeName();
            $this->EmployeeEmail();
            $this->EmployeeContactNumber();
        }
    }

?>