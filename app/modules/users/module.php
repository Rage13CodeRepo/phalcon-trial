<?php

    namespace App\Modules\Users;

    use Phalcon\Loader;
    use Phalcon\Mvc\View;
    use Phalcon\Mvc\Dispatcher;
    use Phalcon\DiInterface;
    use Phalcon\Mvc\ModuleDefinitionInterface;

    /**
     * 
     */

    class Module implements ModuleDefinitionInterface {

        public function registerAutoloaders(DiInterface $di = null) {
            $loader = new Loader();
            $loader->registerNamespaces([
                'App\Modules\Users\Controllers' => APP_PATH . '/modules/users/controllers',
                'App\Modules\Users\Forms'=> APP_PATH . '/modules/users/forms'
            ]);
            $loader->register();
        }

        public function registerServices(DiInterface $di) {
            $di->set('dispatcher', function() {
                $dispatcher = new Dispatcher();
                $dispatcher->setDefaultNamespace('App\Modules\Users\Controllers');
                return $dispatcher;
            });

            $di->set('view', function() {
                $view = new View();
                $view->setViewsDir(APP_PATH . '/modules/users/views');
                $view->setMainView(APP_PATH . '/templates/layouts/users');
                return $view;
            });
        }

    }