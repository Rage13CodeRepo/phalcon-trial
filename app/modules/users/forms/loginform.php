<?php

    namespace App\Modules\Users\Forms;

    use App\Library\BaseForm;
    use Phalcon\Forms\Element\Text;
    use Phalcon\Forms\Element\Submit;
    use Phalcon\Forms\Element\Password;
    use Phalcon\Validation\Validator\PresenceOf;
    use Phalcon\Validation\Validator\Email;
    

    class LoginForm extends BaseForm {
        private function EmployeeEmail() {
            $element = new Text('emp_email');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','Email');
            $element->setLabel('Email');
            $element->addValidator(
                new PresenceOf(['message' => ' Please enter your email to login'])
            );
            $element->addValidator(
                new Email(['message' => " You haven't entered a valid email address"])
            );
            $this->add($element);
        }

        private function EmployeePassword() {
            $element = new Password('emp_password');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','Password');
            $element->setLabel('Password');
            $element->addValidator(
                new PresenceOf(['message' => ' Please enter your password to login'])
            );
            $this->add($element);
        }

        private function Submit() {
            $element = new Submit('submit');
            $element->setAttribute('class','btn btn-primary col-md-12');
            $element->setAttribute('id','submit');
            $element->setAttribute('value','Login');
            $this->add($element);
        }

        public function initialize() {
            $this->EmployeeEmail();
            $this->EmployeePassword();
            $this->Submit();
        }
    }