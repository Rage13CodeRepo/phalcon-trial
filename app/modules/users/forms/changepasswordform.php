<?php

    namespace App\Modules\Users\Forms;

    use Phalcon\Forms\Element\Password;
    use Phalcon\Forms\Element\Submit;
    use Phalcon\Validation\Validator\Identical;
    use App\Library\BaseForm;

    class ChangePasswordForm extends BaseForm {

        private function EmployeeOldPassword() {
            $element = new Password('emp_oldPassword');
            $element->setAttribute('class', 'form-control');
            $element->setAttribute('id', 'OldPassword');
            $element->setLabel('Old Password');
            $this->add($element);
        }
        
        private function EmployeeNewPassword() {
            $element = new Password('emp_newPassword');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','password');
            $element->setLabel('New Password');
            $this->add($element);
        }

        private function EmployeeConfirmNewPassword() {
            $element = new Password('emp_confirmNewPassword');
            $element->setAttribute('class','form-control');
            $element->setAttribute('id','confirmpassword');
            $element->setLabel('Confirm New Password');
            $this->add($element);
        }

        private function Submit() {
            $element = new Submit('submit');
            $element->setAttribute('class','btn btn-primary');
            $element->setAttribute('id','submit');
            $this->add($element);
        }
        
        public function initialize() {
            $this->EmployeeOldPassword();
            $this->EmployeeNewPassword();
            $this->EmployeeConfirmNewPassword();
            $this->Submit();
        }
    }
