<?php 

    namespace App\Modules\Users\Controllers;

    use App\Model\Employee;
    use App\Library\ControllerBase;
    use App\Modules\Users\Forms\ChangePasswordForm;

class ProfileController extends ControllerBase {
        
        public function indexAction() {

        }

        public function changePasswordAction() {
            $form = new ChangePasswordForm();
            $errors = [];
            if($this->request->getPost()) {
                if($form->isValid($this->request->getPost()) != false) {
                    echo $oldPassword = $this->request->getPost('emp_oldPassword');
                    echo $newPassword = $this->request->getPost('emp_newPassword');
                    echo $confirmNewPassword = $this->request->getPost('emp_confirmNewPassword');
                    if($newPassword === $confirmNewPassword && $newPassword != $oldPassword) {
                        $employeeObj = Employee::findFirst($this->SessionData->getUserData('UserId'));
                        if($employeeObj) {
                            if($this->security->checkHash($oldPassword, $employeeObj->emp_password)) {
                                $employeeObj->setEmp_password($this->security->hash($newPassword));
                                $employeeObj->save();
                                $this->response->redirect('users/profile/changepassword');
                            }
                        }
                    }
                }
                else {
                    $errors = $form->getMessage();
                }
            }
            $this->view->errors = $errors;
            $this->view->form = $form;
        }

        public function viewprofileAction() {
            $employeeObj = Employee::findFirst($this->SessionData->getUserData('UserId'));
            $this->view->setVar('employeeObj', $employeeObj);
        }
    }