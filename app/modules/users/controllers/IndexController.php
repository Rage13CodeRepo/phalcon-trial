<?php

    namespace App\Modules\Users\Controllers;

    use App\Library\ControllerBase;
    use App\Library\Utility;
    use App\Model\Employee;
    use App\Modules\Users\Forms\LoginForm;

class IndexController extends ControllerBase {

        public function indexAction() {
            // echo '--  '.$this->security->hash('User@123').'  --';
            // $this->cookies->set('User', 'My very secure cookie', time() + 365 * 86400);
            // $this->session->set('SessionsData', 'Wabba Lubba Dub Dub');
            // die();
            $this->disptacher->forward(['controller' => 'index','action' => 'login']);
        }

        public function homeAction() {
            $empId = $this->SessionData->getUserData('UserId');
            $employeeObj = Employee::findFirst($empId);
            $empname = $employeeObj->getEmp_name();
            $this->view->empname = $empname;
        }

        public function loginAction() {
            if(self::_checkIfUserExists($this->SessionData->getUserData('UserId'))) $this->response->redirect('users/home');

            $form = new LoginForm();
            if($this->request->isPost()) {
                if($form->isValid($this->request->getPost()) != false) {
                    $email = $this->request->getPost('emp_email');
                    $password = $this->request->getPost('emp_password');
                    $employeeObj = Employee::findFirst([
                        'conditions' => "emp_email = '".$email."'"
                    ]);
                    if($employeeObj) {
                        if($this->security->checkHash($password, $employeeObj->emp_password)) {
                            $UserData = [
                                'UserId' => $employeeObj->id,
                                'UserName' => $employeeObj->emp_name
                            ];
                            $this->SessionData->setUserData($UserData);
                            $this->cookies->set('UserData', $UserData, time() + 365 * 86400);
                            $this->response->redirect('users/home');
                        }
                        else {
                            $this->flashSession->error('Incorrect password');
                        }
                    }
                    else {
                        $this->flashSession->error('Incorrect email and password');
                    }
                }
                else {
                    $errors = $form->getMessages();
                }
            }
            $this->view->errors = $errors;
            $this->view->form = $form;
        }

        private function _checkIfUserExists($id) {
            echo $count = Employee::count($id);
            if($count === 1) return true;
            return false;
        }

        public function logoutAction() {
            $this->session->remove('UserData');
            $this->response->redirect('users');
        }
    }