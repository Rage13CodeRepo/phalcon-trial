<?php

    namespace App\Modules\Users\Controllers;
    
    use App\Model\Task;
    use App\Model\Taskassigned;
    use App\Library\ControllerBase;
    use App\Library\Utility;
    use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;

    class TaskController extends ControllerBase {
        
        public function indexAction() {
            
        }
        
        public function viewassignedtaskAction() {
            $employeeId = $this->SessionData->getUserData('UserId');
            // $taskAssignedObj = Taskassigned::getTaskAssignedTo($employeeId);
            // $taskAssignedObj = Taskassigned::find([
            //     'conditions' => 'employee_id = '. $employeeId
            // ]);
            // $taskAssignedObj = $taskAssignedObj->count() === 0 ? null : $taskAssignedObj;
            // $this->view->taskAssignedObj = $taskAssignedObj;
            $currentPage = $this->request->get('page') ? $this->request->get('page') : 1;
            $searchFilter = $this->request->get('search') ? '%'.$this->request->get('search').'%': '%%';
            $builder = $this->modelsManager->createBuilder();
            $builder->from('App\Model\Taskassigned');
            $builder->where('employee_id = :employeeId:', ['employeeId' => $employeeId ]);
            $paginator = new PaginatorQueryBuilder([
                'builder' => $builder,
                'limit' => 3,
                'page' => $currentPage
            ]);
            $page = $paginator->getPaginate();
            $this->view->setVars([
                'page' => $page,
                'searchFilter' => $this->request->get('search'),
            ]);
        }

    }