<?php 

    namespace App\Modules\Store\Controllers;
    
    use App\Library\ControllerBase;
    use App\Model\Product;
    use App\Library\Utility;
    use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;

class ProductController extends ControllerBase {
        
        public function indexAction() {
            
        }

        public function viewallAction() {
            $productObj = Product::find([
                'group' => 'product_type'
            ]);
            $currentPage = $this->request->get('page') ? $this->request->get('page') : 1;
            $searchFilter = $this->request->get('search') ? '%'.$this->request->get('search').'%' : '%%';
            $builder = $this->modelsManager->createBuilder();
            $builder->from('App\Model\Product');
            $builder->where('product_name like :searchFilter:', ['searchFilter' => $searchFilter]);
            $paginator = new PaginatorQueryBuilder([
                'builder' => $builder,
                'limit' => 9,
                'page' => $currentPage
            ]);
            $page = $paginator->getPaginate();
            $this->view->setVars([
                'page' => $page,
                'searchFilter' => $this->request->get('search'),
                'productTypeObj' => $productObj
            ]);           
        }

        public function viewAction($id) {
            $productObj = Product::find([
                "conditions" => "id like ". $id
            ]);
            // Utility::VarDump($products);
            $this->view->setVars([
                'productObj' => $productObj,
            ]);
        }

    }