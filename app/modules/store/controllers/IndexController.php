<?php

    namespace App\Modules\Store\Controllers;

    use App\Library\ControllerBase;

    class IndexController extends ControllerBase {
        
        public function indexAction() {
            echo "This is the store index controller";
        }

    }