<?php

    namespace App\Modules\Store;
    use Phalcon\Loader;
    use Phalcon\Mvc\View;
    use Phalcon\Mvc\ModuleDefinitionInterface;
    use Phalcon\Mvc\Dispatcher;
    use Phalcon\DiInterface;


    class Module implements ModuleDefinitionInterface{

        public function registerAutoloaders(DiInterface $di = null) {
            $loader = new Loader();
            $loader->registerNamespaces([
                'App\Modules\Store\Controller' => APP_PATH.'/modules/store/controllers',
                'App\Modules\Store\Forms' => APP_PATH.'/modules/store/forms'
            ]);
            $loader->register();
        }

        public function registerServices(DiInterface $di) {
            $di->set('dispatcher', function() {
                $dispatcher = new Dispatcher();
                $dispatcher->setDefaultNamespace('App\Modules\Store\Controllers');
                return $dispatcher;
            });

            $di->set('view', function() {
                $view = new View();
                $view->setViewsDir(APP_PATH.'/modules/store/views');
                $view->setMainView(APP_PATH.'/templates/layouts/store');
                $view->setPartialsDir(APP_PATH.'/templates/layouts/partials/store/');
                return $view;
            });
        }
    }