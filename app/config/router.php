<?php

    use Phalcon\Mvc\Router\Group as RouterGroup;
use App\Modules\Admin\Module;

$router = $di->getRouter();

    // Define your routes here
    /*
        the parameter for the route can be defined as regular expressions and
        can be assigned a alias that can be used to reffer to the parameter in
        the controller
    */

    $router->setDefaultModule("admin");
    $router->setDefaultController("index");
    $router->setDefaultAction("home");

    // $router->add('/employee/update/([0-9]+)', [
    //     'controller' => 'employee',
    //     'action' => 'update',
    //     'id' => 1
    // ])->setName('update');

    // $router->add('/',[
    //     'module' => 'admin',
    //     'controller' => 'index',
    //     'action' => 'index'
    // ]);

    $router->add('/users',[
        'module' => 'users',
        'controller' => 'index',
        'action' => 'login'
    ]);

    $router->add('/users/login',[
        'module' => 'users',
        'controller' => 'index',
        'action' => 'login'
    ]);

    $router->add('/users/home',[
        'module' => 'users',
        'controller' => 'index',
        'action' => 'home'
    ]);
    $router->add('/users/logout',[
        'module' => 'users',
        'controller' => 'index',
        'action' => 'logout'
    ]);

    $router->add('/users/task',[
        'module' => 'users',
        'controller' => 'task'
    ]);

    $router->add('/users/profile',[
        'module' => 'users',
        'controller' => 'profile',
        'action' => 'index'
    ]);

    $router->add('/users/profile/changepassword',[
        'module' => 'users',
        'controller' => 'profile',
        'action' => 'changepassword'
    ]);

    $router->add('/users/viewassignedtask',[
        'module' => 'users',
        'controller' => 'task',
        'action' => 'viewassignedtask'
    ]);
    
    $router->add('/users/profile/viewprofile',[
        'module' => 'users',
        'controller' => 'profile',
        'action' => 'viewprofile'
    ]);

    $store = new RouterGroup([
        'module' => 'store',
        'controller' => 'index'
    ]);
    
    $store->setPrefix('/store');

    $store->add('', [
        'module' => 'store',
        'controller' => 'index',
        'action' => 'index'
    ]);

    $store->add('/products', [
        'module' => 'store',
        'controller' => 'product',
        'action' => 'viewall'
    ]);

    $store->add('/product/:params', [
        'module' => 'store',
        'controller' => 'product',
        'action' => 'view',
        'params' => 1
    ]);

    // $router->add('/store', [
    //     'module' => 'store',
    //     'controller' => 'index',
    //     'action' => 'index',
    // ]);

    // $router->add('/store/product', [
    //     'module' => 'store',
    //     'controller' => 'product',
    //     'action' => 'index',
    // ]);

    // $users = new RouterGroup([
    //     'module' => 'users',
    //     'controller' => 'index'
    // ]);

    // $users->setPrefix('/users');


    // $users->add('/profile', array(
    //     'module' => 'users',
    //     'controller' => 'index',
    //     'action' => 'index'
    // ));

    // $router->mount($users);

    $router->mount($store);

    $router->handle();