<?php

    use Phalcon\Loader;

    $loader = new Loader();
    /**
     * We register namespaces used by the application here.
     * 
     */
    $loader->registerNamespaces(array(
        'App\Model' => $config->application->modelsDir,
        'App\Forms' => $config->application->formsDir,
        'App\Library' => $config->application->libraryDir,
    ));

    /**
     * We're a registering a set of directories taken from the configuration file
     */

    $loader->registerDirs(
        [
            $config->application->controllersDir,
            $config->application->formsDir,
            $config->application->modelsDir,
            $config->application->libraryDir
        ]
    );

    $loader->register();