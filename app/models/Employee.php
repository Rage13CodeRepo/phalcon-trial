<?php

    namespace App\Model;
    use App\Library\BaseModel;
    use Phalcon\Mvc\Model\Query\Builder;

    class Employee extends BaseModel {
        public function initialize() {
            /**
             * belongsTo() is used to define many to one relationship
             */
            $this->belongsTo('office_id', 'App\Model\Office', 'id', ['alias' => 'office']);
            $this->belongsTo('job_id', 'App\Model\Job', 'id', ['alias' => 'job']);
            $this->hasManyToMany('id','App\Model\Taskassigned','employee_id', 'task_id', 'App\Model\Task', 'id', [ 'alias' => 'tasks']);
        }

        public function getAllEmployees() {
            $queryBuilder = new Builder();
            // addForm() adds a table to the queryBuilder
            // the second parameter can be used to reffer to the table as an alias
            $queryBuilder->addFrom('App\Model\Employee', 'e', [
            ]);
            // columns property helps select the required columns from the tables
            $queryBuilder->columns("id, emp_name, emp_contactNumber");
            $employeeListObj = $queryBuilder->getQuery()->execute();
            return $employeeListObj;
        }

        public static function getEmployee($id) {
            $queryBuilder = new Builder();
            $queryBuilder->columns([
                'emp.*'
            ]);
            $queryBuilder->addFrom('App\Model\Employee', 'emp');
            $queryBuilder->andWhere('emp.id = '.$id);
            $employeeObj = $queryBuilder->getQuery()->execute();
            return $employeeObj;
        }

        public static function getEmployeeDetails($id) {
            // /**
            //  * Using Builder
            //  */
            // $queryBuilder = new Builder();
            // $queryBuilder->columns([
            //     'emp.*',
            //     'ofc.office_name'
            // ]);
            // $queryBuilder->addFrom('App\Model\Employee', 'emp');
            // $queryBuilder->leftJoin('App\Model\Office','emp.office_id = ofc.id','ofc');
            // $queryBuilder->andWhere('emp.id = '.$id);
            // $employeeObj = $queryBuilder->getQuery()->execute();
            $employeeObj = self::find($id);
            return $employeeObj;
        }

        public function deleteEmployee($id) {
            $queryBuilder = new Builder();
            $queryBuilder->addForm('App\Model\Employee','e',[

            ]);
        }

        public function officeName($id) {
            $office = Office::findFirst($id);
            return $office->office_name;
        }

        public function jobTitle($id) {
            $job = Job::findFirst($id);
            return $job->job_title;
        }

        
    }