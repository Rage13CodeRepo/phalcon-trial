<?php

    namespace App\Model;
    
    use App\Library\BaseModel;
    use Phalcon\Mvc\Model\Query\Builder;

    class Task extends BaseModel {

        public function initialize() {
            /**
             * This model has a many to many relation with the task assigned model.
             * the first three parameters are common as the other relations
             * later on we add the other foreign key's present in the other table as well
             * this will help the when we fire a get query as the query is formeed with 
             * inner joins on that model. 
             */
            $this->hasManyToMany('id','App\Model\Taskassigned','task_id', 'employee_id', 'App\Model\Employee', 'id', [ 'alias' => 'employees']);            
        }

        public function getTask() {

        }

        public static function getAllTasks() {
            $tasks = new Task();
            $tasks = Task::find();
            return $tasks;
        }

        public static function getTaskAssignedTo() {
            
        }

        public static function getTaskDetails($id) {
            $taskId = $id;
            $queryBuilder = new Builder();
            $queryBuilder->columns([
                'task.*',
                'taskasg.*',
                'emp.emp_name'
            ]);
            $queryBuilder->addFrom('App\Model\TaskAssigned','taskasg');
            $queryBuilder->leftJoin('App\Model\Task','taskasg.task_id = task.id','task');
            $queryBuilder->leftJoin('App\Model\Employee','taskasg.employee_id = emp.id','emp');
            $queryBuilder->andWhere('taskasg.task_id = '. $taskId);
            $taskDetailsObj = $queryBuilder->getQuery()->execute();

            return $taskDetailsObj;
        }



    }
?>
