<?php

    namespace App\Model;

    use App\Library\BaseModel;
    use Phalcon\Mvc\Model;
    use Phalcon\Mvc\Model\Query\Builder;

    class Office extends BaseModel {

        public function initialize() {
            /**
             *  hasMany is used to define 1-n relationship
             *  the first parameter is field that is to be set as the foreign key 
             *  the second parameter is the table that is to be used to set the foreign key
             *  the third parameter is the field name that is to be used as the foregin key in that table
             */
            $this->hasMany('id', 'App\Model\Employee', 'office_id', ['alias' => 'office']);
        }

        public static function getOfficeList() {
            $queryBuilder = new Builder();
            $queryBuilder->addFrom('App\Model\Office' ,'o');
            $queryBuilder->columns("id, office_name");
            $officeObj = $queryBuilder->getQuery()->execute();
            return $officeObj;
        }
    }