<?php 
    
    namespace App\Model;
    use App\Library\BaseModel;
    use Phalcon\Mvc\Model\Query\Builder;

    class Taskassigned extends BaseModel {

        public function initialize() {
            /**
             * For many to many relationships the model created for MTM has 
             * belongs to relations in this model while the table where the 
             * foreign keys come's from has Many To Many relation.
             */
            $this->belongsTo('employee_id', 'App\Model\Employee', 'id', ['alias' => 'employee']);
            $this->belongsTo('task_id', 'App\Model\Task', 'id', ['alias' => 'task']);
        }

        public static function getAllAssignedTaskList() {
            $tasks = new Taskassigned();
            $tasks = Taskassigned::find();
            return $tasks;
        }

        public static function getEmployeeNameAssignedToTask($id) {
            $taskId = $id;
            $queryBuilder = new Builder();
            $queryBuilder->columns([
                'emp.id',
                'emp.emp_name'
            ]);
            $queryBuilder->addFrom('App\Model\Taskassigned', 'taskasg');
            $queryBuilder->leftJoin('App\Model\Employee', 'emp.id = taskasg.employee_id ', 'emp');
            $queryBuilder->andWhere('taskasg.task_id ='. $taskId);
            $taskAssignedEmployeeObj = $queryBuilder->getQuery()->execute();
            return $taskAssignedEmployeeObj;
        }

        public static function getCountOfAssignee($id) {
            $count = Taskassigned::Count('task_id = '. $id);
            return $count;
        }

        public static function getTaskAssignedTo($employeeId) {
            $queryBuilder = new Builder();
            $queryBuilder->columns([
                '*'
            ]);
            $queryBuilder->addFrom('App\Model\Taskassigned', 'tskasg');
            $queryBuilder->leftJoin('App\Model\Task', 'task.id = tskasg.task_id', 'task');
            $queryBuilder->andWhere('tskasg.employee_id ='. $employeeId);
            $taskAssignedObj = $queryBuilder->getQuery()->execute();
            return $taskAssignedObj; 
        }

    }

?>