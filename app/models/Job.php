<?php 
    
    namespace App\Model;
    use App\Library\BaseModel;

    class Job extends BaseModel {

        public function initialize() {
            $this->hasMany('id','App\Model\Employee','job_id', ['alias' => 'job']);
        }

        public static function getJobList() {
            $jobs = Job::find([
                'columns' => 'id, job_title'
            ]);
            return $jobs;
        }
    }

?>