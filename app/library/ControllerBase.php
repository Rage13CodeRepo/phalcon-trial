<?php

namespace App\Library;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Http\Response;

abstract class ControllerBase extends Controller
{
    public function initialize() {
        $this->assets->addJs("https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js");
        $this->assets->addCss("https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher) {
        $admin = $this->SessionData->getAdminData();
        $users = $this->SessionData->getUserData();
        $moduleName = $dispatcher->getModuleName();
        $controllerName = $dispatcher->getActionName();
        $actionName = $dispatcher->getActionName();
        if(empty($admin) && $moduleName === 'admin' && $controllerName != 'index' && $actionName != 'login') {
            $dispatcher->forward(['controller' => 'index','action' => 'login']);
        }
        else if(empty($users) && $moduleName === 'users' && $controllerName != 'index' && $actionName != 'login') {
            $dispatcher->forward(['controller' => 'index', 'action' => 'login']);
        } 
    }
    
}
