<?php 

    namespace App\Library;

    use Phalcon\Forms\Form;
    use Phalcon\Forms\Element\Select;

    abstract class BaseForm extends Form {
        /**
         * renders the field for text, submit and select input types with errors
         * if any
         * 
         * @param string $name The name for the input field to be rendered
         * @param array $errors Array containing the errors related to the form
         * @param array $data The datato be rendered for select input type
         * 
         * @return Html
         * 
         */
        public function renderField($name, $errors = NULL, $selectData = NULL) {
            $element = $this->get($name);
            echo "<label for = '".$element->getName()."' class = 'form-label'>".$element->getLabel()."</label>";
            if($selectData != NULL) {
                $el = new Select($name, $selectData);
                $el->setAttribute('class', 'form-control');
                $el->setLabel($element->getLabel());
                echo $el->render($name);
            }
            else {
                echo $element->render($name);

            }
            if($errors != NULL) {
                foreach ($errors as $error) {
                    echo $error->getField() === $name ? "<span class = 'alert-danger'>".$error->getMessage().".</span><br>" : '';
                }
            }
        }
    }

?>