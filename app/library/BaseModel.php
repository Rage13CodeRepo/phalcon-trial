<?php

    namespace App\Library;

    use Phalcon\Mvc\Model;

    abstract class BaseModel extends Model {

        /**
         * The __call() is called when the method name can not be found
         * The first parameter is the method name that is called and the 
         * second parameter is the arguments that are passed to the method 
         */
        public function __call($method, $arguments) {
            // for getters
            if(substr($method,0 ,3) == 'get') {
                $getProperty = lcfirst(str_replace('get', '', $method));
                return $this->{$getProperty};     
            }
            
            // for setters
            if(substr($method, 0, 3) == 'set') {
                $setProperty = lcfirst(str_replace('set', '', $method));
                return $this->{$setProperty} = isset($arguments[0]) ? $arguments[0] :null;
            }

        }
        

    }

?>