<?php

    namespace App\Library;

    class Session {

        public function __call($method, $arguments) {
            /**
             * The session variable should have every first letter capital
             * The data should be stored in an array
             */
            $session = \Phalcon\DI::getDefault()->getSession();
            $sessionVariable = substr($method, 3);
            $sessionData = $arguments ? $arguments[0] : null;
            $methodType = substr($method, 0, 3);
            if($methodType == 'get') {
                if(isset($sessionVariable) && isset($sessionData))
                    return $session->get($sessionVariable)[$sessionData];
                else
                    return $session->get($sessionVariable);
            }
            else if($methodType == 'set') {
                if(is_array($sessionData))
                    $session->set($sessionVariable, $sessionData);
            }
        }
    }