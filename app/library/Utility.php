<?php

    namespace App\Library;

    use App\Model\Employee;

    class Utility {
        public static function message() {
            echo "Hello World !";
        }

        public static function checkForEmployee($id) {
            $employeeObj = Employee::findFirst($id);
            if($employeeObj) return $employeeObj; else return null;
        }

        public static function PrintR($data) {
            echo "<pre>";
            print_r($data);
            echo "</pre>";
            die();
        }

        public static function VarDump($data) {
            echo "<pre>";
            var_dump($data);
            echo "</pre>";
            die();
        }

    }